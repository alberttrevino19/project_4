#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "threads/malloc.h"
#include "threads/synch.h"

/* Identifies an inode. */
#define INODE_MAGIC 0x494e4f44
/* Maximum number of data blocks stored directly in inode */
#define MAX_DB 10
/* Maximum number of sector numbers that can be stored on a sector */
#define MAX_SECTORS 128


/* On-disk inode.
   Must be exactly BLOCK_SECTOR_SIZE bytes long. */
struct inode_disk
  {
    off_t length;                       /* File size in bytes. */
    unsigned magic;                     /* Magic number. */
    /* Extensible Additions (Fast File System) */
    block_sector_t db[MAX_DB];          /* First 10 data blocks (db) */
    block_sector_t fl;              /* First-level indirect block */
    block_sector_t sl;              /* Second-level indirect block */

    bool isDir;         /* marks whether this inode is a directory or a file
                            true == directory, false == normal file */

    uint32_t unused[MAX_SECTORS - 15];
  };

/* On-disk data for an indirect block */
// McCray driving
struct indirect_block_disk
{
  block_sector_t sectors[MAX_SECTORS]; /* Sectors pointed to by this indirect block */
};

/* Returns the number of sectors to allocate for an inode SIZE
   bytes long. */
static inline size_t
bytes_to_sectors (off_t size)
{
  return DIV_ROUND_UP (size, BLOCK_SECTOR_SIZE);
}

/* In-memory inode. */
struct inode
  {
    struct list_elem elem;              /* Element in inode list. */
    block_sector_t sector;              /* Sector number of disk location. */
    int open_cnt;                       /* Number of openers. */
    bool removed;                       /* True if deleted, false otherwise. */
    int deny_write_cnt;                 /* 0: writes ok, >0: deny writes. */
    struct inode_disk data;             /* Inode content. */
    /* Synchronization Additions */
    struct lock extendLock;          /*Lock to protect multiple processes
                                from extending a file at the same time*/
  };

// /* In-memory indirect block */
// // McCray driving
// struct indirect_block
// {
//   block_sector_t sector;              /* Sector number of disk location. */
//   struct indirect_block_disk data;             /* IB content. */
//   struct list_elem elem;              /* Element in IB list. */
// };


/* List of open inodes, so that opening a single inode twice
   returns the same `struct inode'. */
static struct list open_inodes;
// /* List of allocated indirect blocks */
// static struct list id_blocks;


// /* Finds and returns the indirect block that is stored at SECTOR
//    from the id_blocks list. If no such indirect block is in the list,
//    returns NULL. */
// static struct indirect_block*
// get_indirect_block (block_sector_t sector)
// {
//   struct list_elem* e = NULL;
//   struct indirect_block* idBlock = NULL;
//   /* Check whether this inode is already open. */
//   for (e = list_begin (&id_blocks); e != list_end (&id_blocks);
//        e = list_next (e))
//     {
//       idBlock = list_entry (e, struct indirect_block, elem);
//       if (idBlock->sector == sector)
//         {
//           return idBlock;
//         }
//     }
//   // indirect block not found in list
//   return NULL;
// }

/* Returns the block device sector that contains byte offset POS
   within INODE.
   Returns -1 if INODE does not contain data for a byte at offset
   POS. */
static block_sector_t
byte_to_sector (const struct inode *inode, off_t pos)
{
  block_sector_t result = -1;
  // Albert driving
  ASSERT (inode != NULL);
  if(pos == 5120){
    //printf("hello\n");
  }
  if (pos < inode_length(inode))
  {
    //5120 - 5631 pos = 10
    pos = (pos / BLOCK_SECTOR_SIZE);
    // find if pos is in direct blocks or 1st/2nd level indriect blocks
    if (pos < MAX_DB){
      // offest is in direct blocks
      result = inode->data.db[pos];
      return result;
    }
    else if (pos < MAX_DB + MAX_SECTORS){
      // offest is in first-level indirect block
      if(pos == 5120){
        //printf("hellO\n");
      }
      pos -= MAX_DB;
      struct indirect_block_disk* fl = NULL;
      fl = calloc(1, sizeof *fl);
      block_read(fs_device, inode->data.fl, fl);

      ASSERT(fl != NULL);
      result = fl->sectors[pos];
      free(fl);
      if(pos == 0){
        //printf("inode sector: %d\n", inode->sector);
        //printf("sector #: %d\n", result);
      }
      return result;
    }
    else {
      // offest is in second-level inidrect block
      pos -= MAX_DB + MAX_SECTORS;
      struct indirect_block_disk* sl = NULL;
      sl = calloc(1, sizeof *sl);
      block_read(fs_device, inode->data.sl, sl);

      ASSERT(sl != NULL);

      int index = pos / MAX_SECTORS; // index of fl in sl
      pos -= MAX_SECTORS * index;

      struct indirect_block_disk* fl = NULL;
      fl = calloc(1, sizeof *fl);
      block_read(fs_device, sl->sectors[index], fl);

      ASSERT(fl != NULL);
      result = fl->sectors[pos];
      free(fl);
      free(sl);
      return result;
    }
  }
  else{
    return result;
  }
}

/* Initializes the inode module. */
void
inode_init (void)
{
  list_init (&open_inodes);
}

/* allocate space for each sector in the indirect block
  returns true upon completion, false if failure. */
static bool
indirect_allocate(int *currSectors, int totalSectors,
                    struct indirect_block_disk *disk_IB, char* zeros)
  {
    //Albert and McCray Driving
    int numSectorsAlloc = *currSectors;
    //allocate space for each sector in the indirect block
    int sectors_alloc = (numSectorsAlloc - MAX_DB) % MAX_SECTORS; // # of sectors allocated so far
    while(numSectorsAlloc < totalSectors && sectors_alloc < MAX_SECTORS)
    {
      // allocate this sector and store the num in disk_inode->db;
      if (free_map_allocate(1, &disk_IB->sectors[sectors_alloc]))
      {
        block_write(fs_device, disk_IB->sectors[sectors_alloc], zeros);
      }
      else
      {
        return false;
      }
      numSectorsAlloc++;
      *currSectors = numSectorsAlloc;
      sectors_alloc++;
    }
    return true;
}

/* Allocates space in a file at sector I_SECTOR so that it
is NEW_LENGTH in size */
static bool
inode_grow (struct inode_disk* disk_inode, off_t new_length,
  block_sector_t i_sector)
{
  // McCray and Albert driving
  ASSERT(new_length > disk_inode->length);

  // find last sector of old length
  // block_sector_t startSector = inode->sector;
  // block_sector_t endSector = startSector +
  //                             bytes_to_sectors(inode_length(inode));
  off_t old_length = disk_inode->length;
  //printf("old length: %d\n new length: %d\n", old_length, new_length);
  /* # of sectors allocated currently */
  int currSectors = bytes_to_sectors(old_length);
  int totalSectors = bytes_to_sectors(new_length);
  //printf("currSectors: %d\n, totalSectors: %d\n", currSectors, totalSectors);
  static char zeros[BLOCK_SECTOR_SIZE];

  // allocate remaining direct block sectors
  while (currSectors < MAX_DB && currSectors < totalSectors){
    // allocate a new sector at currSectors
    if (free_map_allocate(1, &disk_inode->db[currSectors]))
    {
      block_write(fs_device, disk_inode->db[currSectors], zeros);
      currSectors++;
    }
    else
    {
      return false;
    }
  }



  // allocate remaining 1st-level IB sectors
  struct indirect_block_disk *disk_IB = NULL;
  disk_IB = calloc(1, sizeof *disk_IB);
  if(currSectors == MAX_DB && currSectors < totalSectors){
    // create new indirect block on disk
    //allocate space for disk_IB
    //printf("inode sector: %d\n", i_sector);

    if (!free_map_allocate(1, &disk_inode->fl))
    {
      free(disk_IB);
      return false;
    }
    //printf("Sector #: %d\n", disk_inode->fl);

  }
  else if (currSectors < totalSectors){
    //printf("inode sector: %d\n", i_sector);

    //printf("Sector #: %d\n", disk_inode->fl);
    block_read(fs_device, disk_inode->fl, disk_IB);
  }

  if (currSectors < MAX_DB + MAX_SECTORS && currSectors < totalSectors){
    //allocate space for each sector in the indirect block
    if (indirect_allocate(&currSectors, totalSectors, disk_IB, zeros))
    {
      block_write(fs_device, disk_inode->fl, disk_IB);
    }
    else
    {
      free(disk_IB);
      return false;
    }
  }
  // free indirect block
  free(disk_IB);



  // create new second-level indirect block on disk
  bool needToWriteSL = false; // tracks if sl IB needs to be written/freed
  struct indirect_block_disk *disk_2IB = NULL;
  disk_2IB = calloc(1, sizeof *disk_2IB);

  if((currSectors == (MAX_DB + MAX_SECTORS)) && (currSectors < totalSectors)){
    //allocate space for disk_IB
    if(!free_map_allocate(1, &disk_inode->sl)){
      free(disk_2IB);
      return false;
    }
    needToWriteSL = true;
  }
  else if (currSectors < totalSectors){
    block_read(fs_device, disk_inode->sl, disk_2IB);
    needToWriteSL = true;
  }

  // Allocate remaining 2nd level IB sectors
  /* # of sectors allocated in the 2nd-level IB */
  //printf("currSectors: %d\n", currSectors);
  int level2_index = (currSectors - (MAX_DB + MAX_SECTORS)) / (MAX_SECTORS); //1);
  /* Create and allocated the needed number of 1st-level IBs */
  while(currSectors < totalSectors && level2_index < MAX_SECTORS)
  {
    // create new first-level indirect block on disk
    struct indirect_block_disk *disk_1IB = NULL;
    disk_1IB = calloc(1, sizeof *disk_1IB);

    if ((currSectors - (MAX_DB + MAX_SECTORS)) % 128 == 0) {
      //allocate space for disk_1IB
      if (!free_map_allocate(1, &disk_2IB->sectors[level2_index]))
      {
        free(disk_1IB);
        free(disk_2IB);
        return false;
      }
    }
    else {
      block_read(fs_device, disk_2IB->sectors[level2_index], disk_1IB);
    }

    //allocate space for each sector in the indirect block
    if (indirect_allocate(&currSectors, totalSectors, disk_1IB, zeros))
    {
      block_write(fs_device, disk_2IB->sectors[level2_index], disk_1IB);
    }
    else
    {
      free(disk_1IB);
      free(disk_2IB);
      return false;
    }
    // free indirect block
    free(disk_1IB);
    level2_index++;
  }

  if(needToWriteSL){
    // if still successful, write 2nd-level IB to disk
    block_write(fs_device, disk_inode->sl, disk_2IB);
  }
  // free second-level indirect block
  free(disk_2IB);


  //set new length
  disk_inode->length = new_length;
  block_write(fs_device, i_sector, disk_inode);
  return true;
}

/* Initializes an inode with LENGTH bytes of data and
   writes the new inode to sector SECTOR on the file system
   device.
   Returns true if successful.
   Returns false if memory or disk allocation fails. */
bool
inode_create (block_sector_t sector, off_t length, bool isDir)
{
  struct inode_disk *disk_inode = NULL;
  bool success = false;

  ASSERT (length >= 0);

  /* If this assertion fails, the inode structure is not exactly
     one sector in size, and you should fix that. */
  ASSERT (sizeof *disk_inode == BLOCK_SECTOR_SIZE);

  disk_inode = calloc (1, sizeof *disk_inode);
  if (disk_inode != NULL)
    {
      //size_t sectors = bytes_to_sectors (length);
      disk_inode->length = 0;
      disk_inode->magic = INODE_MAGIC;
      disk_inode->isDir = isDir;
      // McCray, Albert driving
      if (length != 0)
        success = inode_grow(disk_inode, length, sector);
      else
      {
        success = true;
        block_write(fs_device, sector, disk_inode);
      }
      // static char zeros[BLOCK_SECTOR_SIZE];
      //
      // // Allocate 10 data blocks
      // if (sectors > 0)
      // {
      //   int db_allocated = 0; // # of data blocks allocated so far
      //   while(sectors > 0 && db_allocated < MAX_DB)
      //   {
      //     // allocate this sector and store the num in disk_inode->db;
      //     if (free_map_allocate(1, &disk_inode->db[db_allocated]))
      //     {
      //       block_write(fs_device, disk_inode->db[db_allocated], zeros);
      //     }
      //     else
      //     {
      //       success = false;
      //       break;
      //     }
      //     sectors--;
      //     db_allocated++;
      //   }
      // }
      //
      // // Allocated 1st level IB
      // if (sectors > 0 && success)
      // {
      //   // create new indirect block on disk
      //   struct indirect_block_disk *disk_IB = NULL;
      //   //allocate space for disk_IB
      //   disk_IB = calloc(1, sizeof *disk_IB);
      //   free_map_allocate(1, &disk_inode->fl);
      //
      //   //allocate space for each sector in the indirect block
      //   if (indirect_allocate(&sectors, disk_IB, zeros))
      //   {
      //     block_write(fs_device, disk_inode->fl, disk_IB);
      //   }
      //   else
      //   {
      //     success = false;
      //   }
      //   // free indirect block
      //   free(disk_IB);
      // }
      //
      // // Allocate 2nd level IB
      // if (sectors > 0 && success)
      // {
      //   // create new second-level indirect block on disk
      //   struct indirect_block_disk *disk_2IB = NULL;
      //   //allocate space for disk_IB
      //   disk_2IB = calloc(1, sizeof *disk_2IB);
      //   free_map_allocate(1, &disk_inode->sl);
      //
      //   int sls_alloc = 0; // # of sectors allocated in the 2nd-level IB
      //   /* Create and allocated the needed number of 1st-level IBs */
      //   while(sectors > 0 && sls_alloc < MAX_SECTORS && success)
      //   {
      //     // create new first-level indirect block on disk
      //     struct indirect_block_disk *disk_1IB = NULL;
      //     //allocate space for disk_1IB
      //     disk_1IB = calloc(1, sizeof *disk_1IB);
      //     free_map_allocate(1, &disk_2IB->sectors[sls_alloc]);
      //
      //     //allocate space for each sector in the indirect block
      //     if (indirect_allocate(&sectors, disk_1IB, zeros))
      //     {
      //       block_write(fs_device, disk_2IB->sectors[sls_alloc], disk_1IB);
      //     }
      //     else
      //     {
      //       success = false;
      //     }
      //     // free indirect block
      //     free(disk_1IB);
      //   }
      //
      //   // if still successful, write 2nd-level IB to disk
      //   if (success)
      //   {
      //     block_write(fs_device, disk_inode->sl, disk_2IB);
      //   }
      //   // free second-level indirect block
      //   free(disk_2IB);
      // }
      //
      // write disk_inode to the disk

      free (disk_inode);
    }
  return success;
}


/* Reads an inode from SECTOR
   and returns a `struct inode' that contains it.
   Returns a null pointer if memory allocation fails. */
struct inode *
inode_open (block_sector_t sector)
{
  struct list_elem *e;
  struct inode *inode;

  /* Check whether this inode is already open. */
  for (e = list_begin (&open_inodes); e != list_end (&open_inodes);
       e = list_next (e))
    {
      inode = list_entry (e, struct inode, elem);
      if (inode->sector == sector)
        {
          inode_reopen (inode);
          return inode;
        }
    }

  /* Allocate memory. */
  inode = malloc (sizeof *inode);
  if (inode == NULL)
    return NULL;


  /* Initialize. */
  list_push_front (&open_inodes, &inode->elem);
  inode->sector = sector;
  inode->open_cnt = 1;
  inode->deny_write_cnt = 0;
  inode->removed = false;
  lock_init(&inode->extendLock);
  block_read (fs_device, inode->sector, &inode->data);
  /* Allocate and initialize IBs from inode->data */
// McCray, Albert driving
  /* get the number of sectors that this inode should use */
  // int sectorsToFill = bytes_to_sectors(inode_length(inode));
  // if (sectorsToFill > MAX_DB)
  // {
  //   /* First-Level Indirect Block */
  //   fl_ib = malloc(sizeof *fl_ib);
  //   if (fl_ib == NULL)
  //     return NULL;
  //   /* Initialize */
  //   list_push_front(&id_blocks, &fl_ib->elem);
  //   fl_ib->sector = inode->data.fl;
  //   block_read(fs_device, fl_ib->sector, &fl_ib->data);
  //
  //   if (sectorsToFill > MAX_DB + MAX_SECTORS)
  //   {
  //     /* Second-Level Indirect Block */
  //     sl_ib = malloc(sizeof *sl_ib);
  //     if (sl_ib == NULL)
  //       return NULL;
  //     /* Initialize */
  //     list_push_front(&id_blocks, &sl_ib->elem);
  //     sl_ib->sector = inode->data.sl;
  //     block_read(fs_device, sl_ib->sector, &sl_ib->data);
  //
  //     int numSectors = MAX_DB + MAX_SECTORS; /* Total # of sectors initialized
  //                                                 so far */
  //     int i = 0; // index in second level's array of first-level blocks
  //     while (numSectors < sectorsToFill && i < MAX_SECTORS)
  //     {
  //       /* Initialize First-Level indirect block WITHIN second-level block */
  //       fl_ib = NULL;
  //       fl_ib = malloc(sizeof *fl_ib);
  //       if (fl_ib == NULL)
  //         return NULL;
  //       /* Initialize */
  //       list_push_front(&id_blocks, &fl_ib->elem);
  //       fl_ib->sector = sl_ib->data.sectors[i];
  //       block_read(fs_device, fl_ib->sector, &fl_ib->data);
  //
  //       // increment number of sectors initialized and second-level index
  //       numSectors += MAX_SECTORS;
  //       i++;
  //     }
  //   }
  // }

  return inode;
}

/* Reopens and returns INODE. */
struct inode *
inode_reopen (struct inode *inode)
{
  if (inode != NULL)
    inode->open_cnt++;
  return inode;
}

/* Returns INODE's inode number. */
block_sector_t
inode_get_inumber (const struct inode *inode)
{
  return inode->sector;
}

/* Closes INODE and writes it to disk. (Does it?  Check code.)
   If this was the last reference to INODE, frees its memory.
   If INODE was also a removed inode, frees its blocks. */
void
inode_close (struct inode *inode)
{
  /* Ignore null pointer. */
  if (inode == NULL)
    return;
  /* Release resources if this was the last opener. */
  if (--inode->open_cnt == 0)
    {
      /* Remove from inode list and release lock. */
      list_remove (&inode->elem);
      /* Deallocate inode's sector and direct blocks if removed. */
      // McCray driving
      int sectorsRemoved = 0;
      if (inode->removed)
        {
          free_map_release (inode->sector, 1);
          // deallocate direct blocks
          int i = 0;
          while (i < MAX_DB && sectorsRemoved < inode_length(inode))
            {
              free_map_release(inode->data.db[i], 1);
              i++;
              sectorsRemoved++;
            }
        }

      /* Remove indirect block(s) from memory */

      /* First-Level Indirect Block */
      //struct indirect_block* fl_ib = get_indirect_block(inode->data.fl);
      // if (fl_ib != NULL && (MAX_DB < inode_length(inode)))
      // {
      //   // remove from list
      //   list_remove(&fl_ib->elem);
      //   // deallocate from disk if removed
      if (inode->removed && MAX_DB < inode_length(inode))
      {
        // deallocate this IB's sector on disk
        struct indirect_block_disk* fl = NULL;
        fl = calloc(1, sizeof *fl);
        block_read(fs_device, inode->data.fl, fl);
        free_map_release(inode->data.fl, 1);
        // deallocate each sector stored in the indirect block
        int i = 0;
        while (i < MAX_SECTORS && sectorsRemoved < inode_length(inode))
          {
            free_map_release(fl->sectors[i], 1);
            i++;
            sectorsRemoved++;
          }
        free(fl);
      }
      //   block_write(fs_device, fl_ib->sector, &fl_ib->data);
      //   free (fl_ib);
      // }

      // /* Second-Level Indirect Block */
      // struct indirect_block_disk sl = NULL;
      // if (sl_ib != NULL && (MAX_DB + MAX_SECTORS < inode_length(inode)))
      // {
      //   // remove from list
      //   list_remove(&sl_ib->elem);
      //   // deallocate from disk if removed
      if (inode->removed && MAX_DB + MAX_SECTORS < inode_length(inode))
      {
        // deallocate this IB's sector on disk
        struct indirect_block_disk* sl = NULL;
        sl = calloc(1, sizeof *sl);
        block_read(fs_device, inode->data.sl, sl);
        free_map_release(inode->data.sl, 1);
        // deallocate each firect-level IB stored in the indirect block
        int flIndex = 0; // index of each fl indirect block in sl
        struct indirect_block_disk* fl_s = NULL;
        fl_s = calloc(1, sizeof *fl_s);

        //fl_ib = get_indirect_block(sl_ib->data.sectors[flIndex]);
        while ((MAX_DB + (MAX_SECTORS * (flIndex + 1)) < inode_length(inode)) &&
          flIndex < MAX_SECTORS)
        {
          // // remove from list
          // list_remove(&fl_ib->elem);
          block_read(fs_device, sl->sectors[flIndex], fl_s);
          // deallocate this IB's sector on disk
          free_map_release(sl->sectors[flIndex], 1);
          // deallocate each sector stored in the indirect block
          int i = 0;
          while (i < MAX_SECTORS && sectorsRemoved < inode_length(inode))
            {
              free_map_release(fl_s->sectors[i], 1);
              i++;
              sectorsRemoved++;
            }

          //block_write(fs_device, fl_s->sector, &fl_ib->data);
          flIndex++;
        }
      free(fl_s);
      //block_write(fs_device, sl_ib->sector, &sl_ib->data);
      // free (sl_ib);
      free(sl);
    }

      //block_write(fs_device, inode->sector, &inode->data);
      free (inode);
    }
}

/* Marks INODE to be deleted when it is closed by the last caller who
   has it open. */
void
inode_remove (struct inode *inode)
{
  ASSERT (inode != NULL);
  inode->removed = true;
}

/* Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
   Returns the number of bytes actually read, which may be less
   than SIZE if an error occurs or end of file is reached. */
off_t
inode_read_at (struct inode *inode, void *buffer_, off_t size, off_t offset)
{
  uint8_t *buffer = buffer_;
  off_t bytes_read = 0;
  uint8_t *bounce = NULL;

  while (size > 0)
    {
      /* Disk sector to read, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually copy out of this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {
          /* Read full sector directly into caller's buffer. */
          block_read (fs_device, sector_idx, buffer + bytes_read);
        }
      else
        {
          /* Read sector into bounce buffer, then partially copy
             into caller's buffer. */
          if (bounce == NULL)
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }
          block_read (fs_device, sector_idx, bounce);
          memcpy (buffer + bytes_read, bounce + sector_ofs, chunk_size);
        }

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_read += chunk_size;
    }
  free (bounce);

  return bytes_read;
}

/* Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
   Returns the number of bytes actually written, which may be
   less than SIZE if end of file is reached or an error occurs.
   (Normally a write at end of file would extend the inode, but
   growth is not yet implemented.) */
off_t
inode_write_at (struct inode *inode, const void *buffer_, off_t size,
                off_t offset)
{
  //printf("offset: %d\n", offset);
  const uint8_t *buffer = buffer_;
  off_t bytes_written = 0;
  uint8_t *bounce = NULL;

  if (inode->deny_write_cnt)
    return 0;

  // extend file here
  lock_acquire(&inode->extendLock);
  if (offset + size > inode_length(inode))
  {
    // file needs to be extended
    if (!inode_grow(&inode->data, offset + size, inode->sector))
    {
      lock_release(&inode->extendLock);
      return 0;
    }
  }
  lock_release(&inode->extendLock);

  while (size > 0)
    {
      /* Sector to write, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually write into this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {
          /* Write full sector directly to disk. */
          block_write (fs_device, sector_idx, buffer + bytes_written);
        }
      else
        {
          /* We need a bounce buffer. */
          if (bounce == NULL)
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }

          /* If the sector contains data before or after the chunk
             we're writing, then we need to read in the sector
             first.  Otherwise we start with a sector of all zeros. */
          if (sector_ofs > 0 || chunk_size < sector_left)
            block_read (fs_device, sector_idx, bounce);
          else
            memset (bounce, 0, BLOCK_SECTOR_SIZE);
          memcpy (bounce + sector_ofs, buffer + bytes_written, chunk_size);
          block_write (fs_device, sector_idx, bounce);
        }

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_written += chunk_size;
    }
  free (bounce);

  return bytes_written;
}

/* Disables writes to INODE.
   May be called at most once per inode opener. */
void
inode_deny_write (struct inode *inode)
{
  inode->deny_write_cnt++;
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
}

/* Re-enables writes to INODE.
   Must be called once by each inode opener who has called
   inode_deny_write() on the inode, before closing the inode. */
void
inode_allow_write (struct inode *inode)
{
  ASSERT (inode->deny_write_cnt > 0);
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
  inode->deny_write_cnt--;
}

/* Returns the length, in bytes, of INODE's data. */
off_t
inode_length (const struct inode *inode)
{
  return inode->data.length;
}
