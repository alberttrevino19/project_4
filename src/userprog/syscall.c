#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "userprog/process.h"
#include "devices/shutdown.h"
#include "lib/syscall-nr.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "threads/synch.h"
#include "devices/input.h"

#define OPEN_FILES_START_INDEX 2

static void syscall_handler (struct intr_frame *);

struct lock fileLock;

//Albert is driving
/*Helper function that takes in any address and validates it.
Returns true if valid, false otherwise.*/
bool isValidPTR(void * address){
  if(address == NULL || is_kernel_vaddr(address) ||
    pagedir_get_page(thread_current()->pagedir, address) == NULL){
    return false;
  }

  return true;
}

//Jack is driving
/*Helper function that takes in a file decriptor and ensures that it is
within the bounds of the file array. Returns true if within those bounds,
false otherwise*/
bool isValidFD(int fd){
  if(fd < 0 || fd > 129)
    return false;
  return true;
}

void
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&fileLock);
}

/* Terminates Pintos by calling shutdown_power_off()
(declared in devices/shutdown.h). This should be seldom used, because you lose
some information about possible deadlock situations, etc. */
void
halt (void)
{
  //McCray is driving
  shutdown_power_off();
}

/* Terminates the current user program, returning status to the kernel.
If the process's parent waits for it (see below), this is the status that will
be returned. Conventionally, a status of 0 indicates success and nonzero values
indicate errors. */
void
exit (int status)
{
  //Albert, McCray, Jack driving
  struct thread* thisThread = thread_current();
  thisThread->exit_status = status;
  thread_exit();
}

/*Runs the executable whose name is given in cmd_line, passing any given
arguments, and returns the new process's program id (pid). Must return pid -1,
which otherwise should not be a valid pid, if the program cannot load or run
for any reason. Thus, the parent process cannot return from the exec until it
knows whether the child process successfully loaded its executable.
You must use appropriate synchronization to ensure this.

Our thought process:

run executable in commmand line
pass arguments in command line

parent process cannot return until child process successfully load
executable child process must return -1, in the case program cannot load/run

 "fork" and execute process in new thread
 thread_create calls start_process which belongs to child

this is our 'child', cannot return from exec until we know that this has
executed correctly or not*/

pid_t
exec (const char* cmd_line)
{
  //McCray and Albert driving
  bool lineValid = isValidPTR((void*) cmd_line);
  if(!lineValid){
    exit(-1);
  }
  // check that end of 'cmd_line' is still in user space
  char* cmd_line_copy = cmd_line;
  do {
    if (cmd_line_copy[0] == '\0')
      break;
    cmd_line_copy++;
    lineValid = isValidPTR((void*) cmd_line_copy);
  } while(lineValid);
  if (!lineValid)
    exit(-1);

  tid_t pid = process_execute(cmd_line);
  //if thread create doesn't execute correctly return -1
  if(pid == TID_ERROR){
    return -1;
  }
  //else return tid
  return pid;
}

int
wait (pid_t pid)
{
  //McCray is driving
  return process_wait(pid);
}

/*Creates a new file called file initially initial_size bytes in size. Returns
true if successful, false otherwise. Creating a new file does not open it:
opening the new file is a separate operation which would require a open system
call.*/
bool
create (const char *file, unsigned initial_size)
{
  //Albert is driving
  bool fileValid = isValidPTR((void*) file);
  if(!fileValid){
    exit(-1);
  }
  // check that end of 'file' is still in user space
  char* file_copy = file;
  do {
    if (file_copy[0] == '\0')
      break;
    file_copy++;
    fileValid = isValidPTR((void*) file_copy);
  } while(fileValid);
  if (!fileValid)
    exit(-1);

  lock_acquire(&fileLock);
  bool result = filesys_create(file, initial_size);
  lock_release(&fileLock);
  return result;
}

/*Deletes the file called file. Returns true if successful, false otherwise.
A file may be removed regardless of whether it is open or closed, and removing
an open file does not close it. See Removing an Open File, for details.*/
bool
remove (const char *file)
{
  //Albert is driving
  bool fileValid = isValidPTR((void*) file);
  if(!fileValid){
    exit(-1);
  }
  // check that end of 'file' is still in user space
  char* file_copy = file;
  do {
    if (file_copy[0] == '\0')
      break;
    file_copy++;
    fileValid = isValidPTR((void*) file_copy);
  } while(fileValid);
  if (!fileValid)
    exit(-1);

  lock_acquire(&fileLock);
  bool result = filesys_remove(file);
  lock_release(&fileLock);
  return result;
}

/*Opens the file called file. Returns a nonnegative integer handle called a
"file descriptor" (fd) or -1 if the file could not be opened.
File descriptors numbered 0 and 1 are reserved for the console:
fd 0 (STDIN_FILENO) is standard input, fd 1 (STDOUT_FILENO) is standard output.
The open system call will never return either of these file descriptors, which
are valid as system call arguments only as explicitly described below.

Each process has an independent set of file descriptors. File descriptors are
not inherited by child processes.

When a single file is opened more than once, whether by a single process or
different processes, each open returns a new file descriptor. Different file
descriptors for a single file are closed independently in separate calls to
close and they do not share a file position.*/
int
open (const char *file)
{
  //Albert driving
  bool fileValid = isValidPTR((void*) file);
  if(!fileValid){
    exit(-1);
  }
  // check that end of 'file' is still in user space
  char* file_copy = file;
  do {
    if (file_copy[0] == '\0')
      break;
    file_copy++;
    fileValid = isValidPTR((void*) file_copy);
  } while(fileValid);
  if (!fileValid)
    exit(-1);

  lock_acquire(&fileLock);
  struct file* opened_file = filesys_open(file);
  //if file cannot be opened, immediately return
  if(opened_file == NULL){
    lock_release(&fileLock);
    return -1;
  }
  struct file** ofiles = thread_current()->open_files;
  //find the first available index in array and put it There
  int index = OPEN_FILES_START_INDEX;
  while(ofiles[index] != NULL){
    index++;
    if (index == NUMBER_OF_FILES_LIMIT)
      exit(-1);
  }
  ofiles[index] = opened_file;
  lock_release(&fileLock);
  return index;
}

/*Returns the size, in bytes, of the file open as fd.*/
int
filesize (int fd)
{
  // Albert is driving
  if(!isValidFD(fd))
    exit(-1);
  lock_acquire(&fileLock);
  int file_size = file_length(thread_current()->open_files[fd]);
  lock_release(&fileLock);
  return file_size;
}

/*Reads size bytes from the file open as fd into buffer. Returns the number
of bytes actually read (0 at end of file), or -1 if the file could not be
read (due to a condition other than end of file). fd 0 reads from the keyboard
using input_getc().*/
int
read (int fd, void *buffer, unsigned size)
{
  // Albert, Will, McCray driving
  if(!isValidFD(fd) || fd == 1)
    exit(-1);

  bool bufValid = isValidPTR(buffer);
  if(!bufValid){
    exit(-1);
  }
  // check every pg_size that buffer is still valid
  char* buf_copy = (char*) buffer + PGSIZE;
  while(buf_copy < (char*)buffer + size)
  {
    bufValid = isValidPTR((void*) buf_copy);
    if (!bufValid)
      exit(-1);
    buf_copy += PGSIZE;
  }
  // check that end of 'buffer' is still in user space
  buf_copy = (char*)buffer + size;
  bufValid = isValidPTR((void*) buffer);
  if (!bufValid)
    exit(-1);

  lock_acquire(&fileLock);
  if(fd == 0){
    //char keyboardInput[size];
    //unsigned mySize = size;
    uint8_t *casted_buffer = (uint8_t *) buffer;
    int index = 0;
    while (index < size) {
      casted_buffer[index++] = input_getc();
    }
    return size;
  }
  int result = file_read(thread_current()->open_files[fd], buffer, size);
  lock_release(&fileLock);
  return result;
}

/*Writes size bytes from buffer to the open file fd. Returns the number of
bytes actually written, which may be less than size if some bytes could not
be written.
Writing past end-of-file would normally extend the file, but file growth is not
implemented by the basic file system. The expected behavior is to write as many
bytes as possible up to end-of-file and return the actual number written, or 0
if no bytes could be written at all.

fd 1 writes to the console.*/
int
write (int fd, const void *buffer, unsigned size)
{
  // Albert and McCray driving
  if(!isValidFD(fd) || fd == 0)
    exit(-1);

  bool bufValid = isValidPTR(buffer);
  if(!bufValid){
    exit(-1);
  }
  // check every pg_size that buffer is still valid
  char* buf_copy = (char*) buffer + PGSIZE;
  while(buf_copy < (char*)buffer + size)
  {
    bufValid = isValidPTR((void*) buf_copy);
    if (!bufValid)
      exit(-1);
    buf_copy += PGSIZE;
  }
  // check that end of 'buffer' is still in user space
  buf_copy = (char*)buffer + size;
  bufValid = isValidPTR((void*) buf_copy);
  if (!bufValid)
    exit(-1);

  //check pointer
  lock_acquire(&fileLock);
  int bytes_written;
  if(fd == 1){ //are we writing to console?
    putbuf(buffer, size);
    bytes_written = size;
  }
  else{ //just basic write
    struct file *file = thread_current()->open_files[fd];
    if(file != NULL)
      bytes_written = file_write(file, buffer, size);
    else
      bytes_written = -1;
  }

  lock_release(&fileLock);
  return bytes_written;
}

/* Changes the next byte to be read or written in open file fd to position,
expressed in bytes from the beginning of the file. (Thus, a position of 0 is the
file's start.) A seek past the current end of a file is not an error. A later
read obtains 0 bytes, indicating end of file. A later write extends the file,
filling any unwritten gap with zeros. (However, in Pintos, files will have a
fixed length until project 4 is complete, so writes past end of file will return
an error.) These semantics are implemented in the file system and do not require
any special effort in system call implementation.*/
void
seek (int fd, unsigned position)
{
  //McCray driving
  if(!isValidFD(fd))
    exit(-1);
  lock_acquire(&fileLock);
  file_seek(thread_current()->open_files[fd], position);
  lock_release(&fileLock);
}

/* Returns the position of the next byte to be read or written in open file fd,
expressed in bytes from the beginning of the file.*/
unsigned
tell (int fd)
{
  //Albert is driving
  if(!isValidFD(fd))
    exit(-1);
  lock_acquire(&fileLock);
  unsigned result = file_tell(thread_current()->open_files[fd]);
  lock_release(&fileLock);
  return result;
}


/* Closes file descriptor fd. Exiting or terminating a process implicitly closes
all its open file descriptors, as if by calling this function for each one.*/
void
close (int fd)
{
  //Albert, McCray driving
  if(!isValidFD(fd))
    exit(-1);
  lock_acquire(&fileLock);
  file_close(thread_current()->open_files[fd]);
  thread_current()->open_files[fd] = NULL;
  lock_release(&fileLock);
}

static void
syscall_handler (struct intr_frame *f)
{

  //McCray, Will, Albert, Jack driving

  //make copy of esp
  char *myESP = (char*) (f->esp); // temp stack pointer

  //check validation of stack pointer
  bool validStack = isValidPTR(f->esp);
  if(!validStack){
    exit(-1);
  }

  // get syscall number
  int callNum = *myESP;

  //all syscalls with zero arguments
  switch (callNum) {
      case SYS_HALT:
        halt();
        return;
  }


  int firstArg = * (int *) (myESP + sizeof(int));
  //check if valid
  bool firstValid = !is_kernel_vaddr((myESP + sizeof(int)));
  if(!firstValid){
    exit(-1);
  }

  //all the syscalls with one argument
  switch (callNum){
      case SYS_EXIT:
        exit(firstArg);
        return;
      case SYS_EXEC:
        f->eax = exec((const char *) firstArg);
        return;
      case SYS_WAIT:
        f->eax = wait((pid_t) firstArg);
        return;
      case SYS_REMOVE:
        f->eax = remove((const char *) firstArg);
        return;
      case SYS_OPEN:
        f->eax = open((const char*) firstArg);
        return;
      case SYS_FILESIZE:
        f->eax = filesize(firstArg);
        return;
      case SYS_TELL:
        f->eax = tell(firstArg);
        return;
      case SYS_CLOSE:
        close(firstArg);
        return;
  }

  int secondArg = * (int *) (myESP + 2 * sizeof(int));
  //check if valid
  bool secondValid = !is_kernel_vaddr((myESP + sizeof(int)));
  if(!secondValid){
    exit(-1);
  }

  //all syscalls with two arguments
  switch (callNum) {
      case SYS_CREATE:
        f->eax = create((const char *) firstArg, (unsigned) secondArg);
        return;
      case SYS_SEEK:
        seek((int) firstArg, (unsigned) secondArg);
        return;
  }


  int thirdArg = * (int *) (myESP + 3 * sizeof(int));
  //check if valid
  bool thirdValid = !is_kernel_vaddr((myESP + sizeof(int)));
  if(!thirdValid){
    exit(-1);
  }
  //all the syscalls with three arguments
  switch (callNum){
    case SYS_READ:
      f->eax = read(firstArg, (void *) secondArg, (unsigned) thirdArg);
      return;
    case SYS_WRITE:
      f->eax = write((int) firstArg, (const void *) secondArg,
        (unsigned) thirdArg);
      return;
  }
}
