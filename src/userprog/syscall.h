#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include <lib/user/syscall.h>

void syscall_init (void);
bool isValidPTR(void * address);
bool isValidFD(int fd);

#endif /* userprog/syscall.h */
